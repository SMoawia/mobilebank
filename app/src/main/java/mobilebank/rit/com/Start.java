package mobilebank.rit.com;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.os.Handler;

public class Start extends Base {

    private static int SPLASH_TIME_OUT = 5000;
    ProgressBar spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        runLoadingThread();
    }

    private void runLoadingThread() {
        RunSpinner();
        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                doneSplash();
                StopSpinner();
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void doneSplash() {
        Intent i = new Intent(Start.this, Home.class);
        //putLoadedData(i, result);
        startActivity(i);
        finish();
    }

    private void putLoadedData(Intent i, String result) {
        //i.putExtra("Data", result.toString());
    }

    //AsyncHttpClient client = new AsyncHttpClient();
    //RequestParams params = new RequestParams();
    //params.put("UserName", UserName.toString());
    //client.get(c.Url + "/Home/Home", params, new AsyncHttpResponseHandler() {
    //   @Override
    //    public void onSuccess(String data) {
    //        while (true) {
    //            if ((System.currentTimeMillis() - startTime) > SPLASH_TIME_OUT) {
    //                break;
    //            }
    //        }
    //        doneSplash(data);
    //    }
    // });
}
